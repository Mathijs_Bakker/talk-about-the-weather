pub fn compass_azimuth(deg: i32) -> &'static str {
    match deg {
        349..=360 => return "N",
        00..=11 => return "N", 
        12..=34 => return "NNE",
        35..=56 => return "NE",
        57..=79 => return "ENE",
        80..=101 => return "E",
        102..=124 => return "ESE",
        125..=146 => return "SE",
        147..=169 => return "SSE",
        170..=191 => return "S",
        192..=214 => return "SSW",
        215..=236 => return "SW",
        237..=259 => return "WSW",
        260..=281 => return "W",
        282..=304 => return "WNW",
        305..=326 => return "NW",
        327..=348 => return "NNW",
        _ => return "Error while matching a wind direction",
    }
}
pub fn arrow_icon(deg: i32) -> &'static str {
    match deg {
        338..=360 => return "",
        00..=23 => return "", 
        24..=68 => return "",
        69..=112 => return "",
        113..=157 => return "",
        158..=202 => return "",
        203..=247 => return "",
        248..=292 => return "",
        293..=337 => return "",
        _ => return "Error while matching a wind direction.",
    }
}
pub fn speed_to_beaufort(meter_per_sec: f64) -> &'static str {
    match meter_per_sec {
        s if s > 32.7 => return "12",
        s if s > 28.5 => return "11",
        s if s > 24.5 => return "10",
        s if s > 20.8 => return "9",
        s if s > 17.2 => return "8",
        s if s > 13.9 => return "7",
        s if s > 10.8 => return "6",
        s if s > 8.0 => return "5",
        s if s > 5.5 => return "4",
        s if s > 3.4 => return "3",
        s if s > 1.6 => return "2",
        s if s > 0.4 => return "1",
        s if s <= 0.4 => return "0",
        _ => return "Error matching a Beaufort value (Bft).", 
    }
}

pub fn speed_to_beaufort_icon(meter_per_sec: f64) -> &'static str {
    match meter_per_sec {
        s if s > 32.7 => return "",
        s if s > 28.5 => return "",
        s if s > 24.5 => return "",
        s if s > 20.8 => return "",
        s if s > 17.2 => return "",
        s if s > 13.9 => return "",
        s if s > 10.8 => return "",
        s if s > 8.0 => return "",
        s if s > 5.5 => return "",
        s if s > 3.4 => return "",
        s if s > 1.6 => return "",
        s if s > 0.4 => return "",
        s if s <= 0.4 => return "",
        _ => return "Error matching a Beaufort value (icon).", 
    }
}

pub fn speed_to_knots(meter_per_sec: f64) -> f64 {
    let knot = 1.9438444924;
    (meter_per_sec * knot).round()
}

pub fn speed_to_mph(meter_per_sec: f64) -> f64 {
    let mile_hr = 2.2369362921;    
    (meter_per_sec * mile_hr).round()
}

pub fn speed_to_kmh(meter_per_sec: f64) -> f64 {
    let kmh = 3.6;    
    (meter_per_sec * kmh).round()
}
