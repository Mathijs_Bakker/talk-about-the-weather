mod forecast_model;
mod wind;

use structopt::StructOpt;
use exitfailure::ExitFailure;
use reqwest::Url;

#[derive(StructOpt)]
pub struct Cli {
    city: String,
    country_code: String
}

impl forecast_model::Forecast {
    async fn get(city: &str, country_code: &str) -> Result<Self, ExitFailure> {
        // api.openweathermap.org/data/2.5/weather?q={city name},{state code},{country code}&appid={API key}
        let base_url = "http://api.openweathermap.org/data/2.5/weather?q=";
        let api_key = "&appid=9d84759a373b342eb521585d7822606d";
        let query = format!("{}{},{}{}", base_url, city, country_code, api_key);

        println!("{}", query);

        let url = Url::parse(&*query)?;

        let response = reqwest::get(url)
            .await?
            .json::<forecast_model::Forecast>()
            .await?;
        Ok(response)
    }
}

#[tokio::main]
async fn main() -> Result<(), ExitFailure> {
    let args = Cli::from_args();
    let response = forecast_model::Forecast::get(&args.city, &args.country_code).await?;

    // println!("City: {} and Country Code: {} Wind Speed: {}", args.city, args.country_code, response.wind.speed);
    println!("City: {} and Country Code: {} 煮{} Bft. {}", 
             args.city, args.country_code, 
             wind::speed_to_beaufort(response.wind.speed), 
             wind::compass_azimuth(response.wind.deg));

    println!("{} Bft", wind::speed_to_beaufort_icon(response.wind.speed));
    println!("{} knots", wind::speed_to_knots(response.wind.speed));
    println!("{} mile/hour", wind::speed_to_mph(response.wind.speed));
    println!("{} km/h", wind::speed_to_kmh(response.wind.speed));
    println!("{} arrow", wind::arrow_icon(response.wind.deg));

    println!("{} cloudiness perc", cloudiness_percentage(response.clouds.all));
    println!("{} clouds", cloudiness_symbol(response.clouds.all));

    println!("{:.1$} temp", temp_in_celsius(response.main.temp), 1);
    Ok(())
}

fn temp_in_celsius(temp_fahrenheit: f64) -> f64 {
    (temp_fahrenheit-32.0)/18.0
}

fn cloudiness_percentage(perc: i32) -> i32 {
    perc
}

fn cloudiness_symbol(cloudiness_perc: i32) -> &'static str {
    match cloudiness_perc {
        perc if perc > 90 => return "", // cloudy
        perc if perc > 70 => return "", // cloud
        perc if perc > 50 => return "", // cloudy high
        perc if perc > 30 => return "", // cloudy
        perc if perc > 10 => return "", // sunny overcast
        perc if perc >= 0 => return "", // sunny
       _ => return "error", 
    }
}
