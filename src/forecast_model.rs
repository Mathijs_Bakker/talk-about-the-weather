use serde_derive::Deserialize;

#[derive(Deserialize, Debug)]
pub struct Forecast {
    coord: Coord,
    weather: Weather,
    base: String,
    pub main: Temperatures,
    visibility: i32,
    pub wind: Wind, 
    pub clouds: Clouds,
    dt: i32,
    sys: Sys,
    timezone: i32,
    id: i32,
    name: String,
    cod: i32,
}

#[derive(Deserialize, Debug)]
struct Coord {
    lon: f64,
    lat: f64,
}

#[derive(Deserialize, Debug)]
struct Weather {
    details: WeatherDetails,
}

#[derive(Deserialize, Debug)]
struct WeatherDetails {
    id: i32,
    main: String,
    description: String,
    icon: String,
}

#[derive(Deserialize, Debug)]
pub struct Temperatures {
    pub temp: f64,
    feels_like: f64,
    temp_min: f64,
    temp_max: f64,
    pressure: i32,
    humidity: i32,
}

#[derive(Deserialize, Debug)]
pub struct Wind {
    pub speed: f64,
    pub deg: i32,
}


#[derive(Deserialize, Debug)]
pub struct Clouds {
    pub all: i32,
}

#[derive(Deserialize, Debug)]
struct Sys {
    r#type: f64,
    id: i32, 
    country: String,
    sunrise: i32,
    sunset: i32,
}
